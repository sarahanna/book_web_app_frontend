import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ActivatedRoute, Params} from "@angular/router";
import { Book } from "../books.model";
import { BookService } from "../books.service";

@Component({
  templateUrl:'detail-book.component.html',
  selector:'app-detail',
  styleUrls:['detail-book.component.css']
})
export class DetailComponent implements OnInit{

  public book!:Book

  constructor(public detailBookService:BookService,
    @Inject(MAT_DIALOG_DATA) public id:number
   ){

   }

  ngOnInit(){
    console.log(this.id)

    this.detailBookService.getBook(this.id)
    .subscribe((data)=>{
      console.log(data)
    this.book = data
      console.log(this.book)
    })


}

}
