export interface Book{
  id:number,
  title:string,

  category:string,
  sdesc:string,
  published:boolean

}
