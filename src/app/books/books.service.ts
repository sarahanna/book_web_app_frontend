

// //rxjs(in place of event emitter ) : objects that help us pass data around
// // import 'subject' from rxjs : subject is actually an event emmitter

// //observable <---- subscription --- observer(next(),error(),complete())
//  // |   |                           ^      ^
//  // |   |-----invoke----------------|      |
//  // --------(wraps callback)http request----
// // observer suscribes the observable (establishes the subscription and manages it)
// // we called next on the subject : and subject considered as the observable

// //observer is something that we pass into subscribe, basically collection of functions that can do something upon the method calls

// //subject : special kind of observable
// // active observable, we can control the data emitting(next())


// //observable : stream of data, that you can manage(as in case of subject,active) or managed for you(as in case of passive, http request)


//service : typescript class

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Book } from "./books.model";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn:'root'
})

export class BookService
{
 private books:Book[]=[]
 private book!:Book
 private booksArray = new Subject<Book[]>()
 host = environment.host
 accessToken !: string


  constructor(private http: HttpClient){}

 //objects and arrays are reference types : if you copy it, that means u r copying the address

 //to retreive books

 gethttp()
 {
  let token = localStorage.getItem('info');
  console.log(token)
 if(token)
 {
   this.accessToken = token
 }
 else
 {
   this.accessToken = ''
 }
 console.log(this.accessToken)
  const httpOptions = {
    headers: new HttpHeaders({
    'x-access-token':this.accessToken
     })
  };
  return httpOptions
 }

 getBooks()
 {
    const options = this.gethttp()
    this.http.get<Book[]>(this.host+'/book',options)
    .subscribe({
      next: (data)=>{
        console.log(data)

             this.books = data

             this.booksArray.next([...this.books])

           },
           error:(error)=>{
             console.log(error)

             const msg : { [key: string]: any } = error
             alert(msg['message'])
           }
    }
     )



 }
 getBook(id:number)
 {
  const options = this.gethttp()
    return this.http.get<Book>(this.host+`/book/${id}`,options)
 }

 //listener function for the Subject
  getBookArrayListener()
{
  return this.booksArray.asObservable()
  // returns an object that we can listen to but not emit
  //we can emit from this file but not from where that received its reference
}

 createBooks(book:Book,pdfFile:FileList)
{
  const options = this.gethttp()
  console.log(options.headers)
  console.log(pdfFile[0])
  const formParams = new FormData();

  formParams.append('pdfFile',pdfFile[0])
  formParams.append('title',book.title)
  formParams.append('category',book.category)
  formParams.append('sdesc',book.sdesc)

  console.log(formParams)
  console.log(typeof(pdfFile))
  console.log(options)
    this.http.post(this.host+'/book',formParams,options)
    .subscribe({
      next:(data)=>{
        console.log(data)
        this.getBooks()
      },
      error: (error)=>{
        console.log(error)
        const msg : { [key: string]: any } = error
        alert(msg['message'])
      }
    }
    )
 }

deleteBooks(id:number)
{
  const options = this.gethttp()
  if(confirm("Are you sure to delete ")) {
  this.http.delete(this.host+`/book/${id}`,options)
  .subscribe({
    next:(data)=>{
    console.log(data)
    this.getBooks()
  },
  error:(error)=>{
    console.log(error)
    alert('error in deleting books, not found')
    }
  }
  )
 }
}

contentOfBook(id:number)
{

  return this.http.get(this.host+`/book/download/${id}`, {
    responseType:'blob',
    headers: new HttpHeaders({
    'x-access-token':this.accessToken
     })})
}

publishBook(book:Book,published:boolean)
{
  const options = this.gethttp()
  console.log(book['published'])
  this.http.patch(this.host+`/book/status/${book.id}`,{published:published},options)
  .subscribe({
    next:(data)=>{
      console.log(data)
      alert('successfully updated status of book')
      this.getBooks()
      },
      error:(error)=>{
        console.log(error)
        alert('error in updating the status,not found')
      }
    }
  )
}

updateBook(book:Book,pdfFile:FileList)
{
  const options = this.gethttp()
  const formParams = new FormData();
  formParams.append("id",(book.id).toString())
  pdfFile == null ? formParams.append("pdfFile",'null') : formParams.append("pdfFile",pdfFile[0])

  formParams.append('title',book.title)
  formParams.append('category',book.category)
  formParams.append('sdesc',book.sdesc)

  console.log(formParams)

  this.http.put(this.host+`/book/${book.id}`,formParams,options)
  .subscribe({
    next:(data)=>{
      console.log(data)
      this.getBooks()
    },
    error:(error)=>{
      console.log(error)
      alert('error in updating the book,not found')
    }
  }
  )
}

}


