import { Component, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { Book } from "../books.model";
import { BookService } from "../books.service";
import { DetailComponent } from "../detail-book/detail-book.component";

@Component({
  selector:'app-list-book',
  templateUrl:'./list-book.component.html',
  styleUrls:['./list-book.component.css']
})

export class ListBookComponent implements OnInit, OnDestroy
{
  displayedColumns: string[] = ['title','author','category','status','delete','download','edit','details'];
  books:Book[]=[]
  book! : Book
  private booksub!: Subscription;
  dataSource!:MatTableDataSource<any>

  //dependency injection
  constructor(public listBookService:BookService,
    private router:Router,public dialog: MatDialog){}
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit() {
    this.listBookService.getBooks()
    //subscribe takes three arguments :
    // 1: function that get emmitted when new data is emitted
    //2 :  called whenever an error is emitted
    //3: function will be called when the observable is completed
    this.booksub = this.listBookService.getBookArrayListener()
    .subscribe({
      next: (books:Book[]) => {
        this.books = books
        this.dataSource = new MatTableDataSource(books)
        console.log('hi')
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error:(error)=>{
        console.log(error)
        const msg : { [key: string]: any } = error
        alert(msg['message'])
      }
    }

  );
}

applyFilter(event: Event)
{
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}

onDelete(id:number)
{
  this.listBookService.deleteBooks(id)
}

onContent(id:number)
{
  console.log('hi')
  this.listBookService.contentOfBook(id)
  .subscribe({
    next:data=>{
      var downloadURL = window.URL.createObjectURL(data);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = `${id}.pdf`;
      link.click();
      },
      error:(error)=>{
        console.log(error)
        alert('error in downloading')
      }
    }
  )
}

onEdit(id:number)
{
  console.log('hi,onEdit')
  this.router.navigate([`/edit/${id}`])
}

onPublish(book:Book,published:boolean)
{
  this.listBookService.publishBook(book,published)
}

onDetails(id:number)
{
  this.router.navigate([`/detail/${id}`])
}

openDialog(id:number)
{
  this.dialog.open(DetailComponent,{data:id});
}

onAddBook()
{
  this.router.navigate([`/create`])
}

logout()
{
  localStorage.setItem('info','0');
// AppComponent.login = false
//  console.log(AppComponent.login)
 console.log('inside logout')
  this.router.navigate(['/login'])

}

ngOnDestroy()
{
  this.booksub.unsubscribe()
}

}