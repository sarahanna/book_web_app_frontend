

// import { Component, EventEmitter, OnInit, Output } from "@angular/core";
// import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
// import { ActivatedRoute, ParamMap, Params, Router } from "@angular/router";
// import { Book } from "../books.model";
// import { BookService } from "../books.service";
// import { Subscription } from "rxjs";

// @Component({
//   selector:'app-create-book',
//   templateUrl:'./create-book.component.html',
//   styleUrls:['./create-book.component.css']
// })

// export class CreateBookComponent implements OnInit{

//   books :Book[] =[]
//   pdfFile!: FileList
//   //private gotbook!:Subscription;
//   mode = 'create'
//    id! :number
//   book! :Book
//   form! : FormGroup
//   //@Output() BookCreated = new EventEmitter()

//   constructor(public createBookService:BookService,public route: ActivatedRoute,private router:Router){}

//   ngOnInit(){
//     this.form = new FormGroup({
//       title:new FormControl(null,{validators:[Validators.required]}),
//       author:new FormControl(null,{validators:[Validators.required]}),
//       phone:new FormControl(null,{validators:[Validators.required,Validators.minLength(10),Validators.maxLength(10)]}),
//       email:new FormControl(null,{validators:[Validators.required,Validators.email]}),
//       category:new FormControl(null,{validators:[Validators.required]}),
//       sdesc:new FormControl(null,{validators:[Validators.required]}),
//       pdfFile:new FormControl(null,{validators:[Validators.required]})

//     })

//     this.route.params.subscribe((params:Params)=>{
//       this.id = params['id']
//      console.log(this.id)
//      if(this.id)
//      {
//       this.mode = 'edit'
//       this.createBookService.getBook(this.id)
//       .subscribe(book=>{
//         this.book= {
//           id:book.id,
//           title:book.title,
//           author:book.author,
//           phone:book.phone,
//           email:book.email,
//           category:book.category,
//           sdesc:book.sdesc,
//           published:book.published
//         }
//         console.log(this.book)

//   this.form.setValue({
//     title:this.book.title,
//     author:this.book.author,
//     phone:this.book.phone,
//     email:this.book.email,
//     category:this.book.category,
//     sdesc:this.book.sdesc,
//     pdfFile:null
//   })

// })
// }
// })

// }

// // getErrorMessage() {
// //   if (this.form.value.email.hasError('required')) {
// //     return 'You must enter a value';
// //   }

// //   return this.form.value.email.hasError('email') ? 'Not a valid email' : '';
// // }

//   onFileSelected(event : Event){
//     const file = (event.target as HTMLInputElement).files
//     this.form.patchValue({pdfFile:file})
//     this.form.get('pdfFile')?.updateValueAndValidity()
//     console.log(file)
//     console.log(this.form)
//   }

//   onCreateBook(){
//     if(this.form.invalid)
//     {
//       return alert('Please fill all the feilds')
//     }
//     console.log(this.form.value)
//     if(this.mode == 'create')
//     {
//       const newBook = {
//         id:0,
//         title:this.form.value.title,
//         author:this.form.value.author,
//         phone:this.form.value.phone,
//         email:this.form.value.email,
//         category:this.form.value.category,
//         sdesc:this.form.value.sdesc,
//         published:false
//       }
//       this.pdfFile =this.form.value.pdfFile
//       console.log(typeof(this.pdfFile))
//       //this.BookCreated.emit(newBook)
//       this.createBookService.createBooks(newBook,this.pdfFile)
//       this.form.reset()
//     }
//     else{
//       console.log(this.id)
//      const updatedBook ={ id:this.id,
//       title:this.form.value.title,
//       author:this.form.value.author,
//       phone:this.form.value.phone,
//       email:this.form.value.email,
//       category:this.form.value.category,
//       sdesc:this.form.value.sdesc,
//       published:false}
//       this.pdfFile =this.form.value.pdfFile
//       console.log(typeof(this.pdfFile))
//       this.createBookService.updateBook(updatedBook,this.pdfFile)
//         this.form.reset()

//     }
//     this.router.navigate(['/'])

//   }

// }
import { Component, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Book } from "../books.model";
import { BookService } from "../books.service";



@Component({
    selector:'app-create-book',
    templateUrl:'./create-book.component.html',
    styleUrls:['./create-book.component.css']
})

export class CreateBookComponent implements OnInit{

    books :Book[] =[]
    pdfFile!: FileList
    mode = 'create'
    private id! :number
    book! :Book
    form! : FormGroup

    constructor(public createBookService:BookService,public route: ActivatedRoute,private router:Router){}

    ngOnInit(){
      this.route.params.subscribe((params:Params)=>{
        this.id = params['id']
        console.log(this.id)
        if(this.id)
        {
         this.mode = 'edit'
         this.createBookService.getBook(this.id)
        .subscribe(book=>{
          this.book= {
          id:book.id,
          title:book.title,

          category:book.category,
          sdesc:book.sdesc,
          published:book.published
        }
        console.log(this.book)
        this.form.setValue({
          title:this.book.title,

          category:this.book.category,
          sdesc:this.book.sdesc,
          pdfFile:null
        })
      })
    }
  })
  console.log(this.mode)
  if(this.mode == 'create')
  {
    console.log('hi')
    this.form = new FormGroup({
    title:new FormControl(null,{validators:[Validators.required]}),
    category:new FormControl(null,{validators:[Validators.required]}),
    sdesc:new FormControl(null,{validators:[Validators.required,Validators.maxLength(250)]}),
    pdfFile : new FormControl(null,{validators:[Validators.required]})
    })
  }
  else{
      console.log('hi,edit')
      this.form = new FormGroup({
        title:new FormControl(null,{validators:[Validators.required]}),
        category:new FormControl(null,{validators:[Validators.required]}),
        sdesc:new FormControl(null,{validators:[Validators.required,Validators.maxLength(250)]}),
        pdfFile : new FormControl(null)
      })
    }
  }

  onFileSelected(event : Event)
  {
    const file = (event.target as HTMLInputElement).files
    this.form.patchValue({pdfFile:file})
    this.form.get('pdfFile')?.updateValueAndValidity()
    console.log(file)
    console.log(this.form)
  }

  onCreateBook()
  {
  if(this.form.invalid)
  {
    return alert('Enter the required feilds')
  }
  console.log(this.form.value)
  if(this.mode == 'create')
  {
    if(confirm("Are you sure you want to create this book ")) {
    const newBook ={
      id:0,
      title:this.form.value.title,

      category:this.form.value.category,
      sdesc:this.form.value.sdesc,
      published:false

    }
    this.pdfFile =this.form.value.pdfFile
    console.log(typeof(this.pdfFile))


    this.createBookService.createBooks(newBook,this.pdfFile)
    this.form.reset()
  }
}
else
{
  console.log(this.id)
  const updatedBook ={ id:this.id,
  title:this.form.value.title,
  category:this.form.value.category,
  sdesc:this.form.value.sdesc,
  published:false}
  this.pdfFile =this.form.value.pdfFile
  console.log(this.pdfFile)
  this.createBookService.updateBook(updatedBook,this.pdfFile)
  this.form.reset()
}
this.router.navigate(['/books'])
}
}

