import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';



import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { CreateBookComponent } from './books/create-book/create-book.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatInputModule} from '@angular/material/input'



import {MatButtonModule} from '@angular/material/button'

import {MatCardModule} from '@angular/material/card'

import {MatExpansionModule} from '@angular/material/expansion'

// import { HeaderComponent } from './header/header.component';



import {MatToolbarModule} from '@angular/material/toolbar';

import { ListBookComponent } from './books/list-book/list-book.component';

import { HeaderComponent } from './header/header.component';



import { NgxMatFileInputModule } from '@angular-material-components/file-input';

import { HttpClientModule } from '@angular/common/http';

import {MatPaginatorModule} from '@angular/material/paginator';

import {MatTableModule} from '@angular/material/table';

import {MatGridListModule} from '@angular/material/grid-list';

import { MatIconModule } from '@angular/material/icon';
import { FooterComponent } from './footer/footer.component';
import {MatSortModule} from '@angular/material/sort';
import { DetailComponent } from './books/detail-book/detail-book.component';

import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';
import { SignupComponent } from './users/signup/signup.component';
import { LoginComponent } from './users/login/login.component';
import { HomeComponent } from './home/home.component';



@NgModule({

  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    SignupComponent,
    LoginComponent,
    CreateBookComponent,
    ListBookComponent,
    DetailComponent,
    FooterComponent
  ],
  imports:[

    BrowserModule,

    AppRoutingModule,


    ReactiveFormsModule,
    MatFormFieldModule,
    MatCardModule,

    MatInputModule,

    MatButtonModule,

    MatToolbarModule,

    MatExpansionModule,

    NgxMatFileInputModule,

    HttpClientModule,

    MatPaginatorModule,

    MatTableModule,

    MatIconModule,

    MatGridListModule,
    MatSortModule,
    MatDialogModule,
    MatMenuModule,



    BrowserAnimationsModule

  ],

  providers: [],

  bootstrap: [AppComponent]

})

export class AppModule { }

