
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
// import { Book } from "./books.model";
import { environment } from "src/environments/environment";
import { AppComponent } from "../app.component";
import { User } from "./users.model";

@Injectable({
  providedIn:'root'
})

export class UserService
{

private author!:User

 host = environment.host

  constructor(private http: HttpClient,private router:Router){}

  signup(author:User)
  {
    const formParams = new FormData();

    formParams.append('author',author.author)
    formParams.append('email',author.email)
    formParams.append('password',author.password)

    this.http.post(this.host+'/signup',formParams)
    .subscribe({
      next:(data)=>{
        console.log(data)
        const msg : { [key: string]: any } = data

        console.log(msg['auth'])

          alert(msg['message'])
          this.router.navigate(['/login'])



      },
      error: (error)=>{
        console.log(error)

        const msg : { [key: string]: any } = error
        console.log(msg)
        alert(msg['message'])
      }
    }
    )
  }

  login(author:User)
  {
    const formParam = new FormData();
    formParam.append('email',author.email)
    formParam.append('password',author.password)
    return this.http.post(this.host+'/login',formParam)


  }

}
