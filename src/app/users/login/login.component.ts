import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AppComponent } from "src/app/app.component";
import { UserService } from "../users.service";

@Component({
  selector:'app-login',
  templateUrl:'./login.component.html',
  styleUrls:['./login.component.css']
})
export class LoginComponent implements OnInit{
  form! : FormGroup
  public clicked = false
  constructor(public UserService:UserService,private router:Router){}
  ngOnInit(){
    // console.log(AppComponent.login)
    this.form = new FormGroup({
    // author:new FormControl(null,{validators:[Validators.required]}),
    password:new FormControl(null,{validators:[Validators.required]}),
    // phone:new FormControl(null,{validators:[Validators.required,Validators.minLength(10),Validators.maxLength(10)]}),
    email:new FormControl(null,{validators:[Validators.required,Validators.email]}),

    })
  }

  onLogin()
  {
    console.log('hello')
    console.log(this.form)
    const author = {
      author:'',
      email:this.form.value.email,
      password:this.form.value.password

    }
    this.UserService.login(author)
    .subscribe({
      next:(data)=>{
        console.log(data)
        const token : { [key: string]: any } = data
        console.log(token)
        console.log(token["accessToken"])
        localStorage.setItem('info',token["accessToken"]);
        this.router.navigate(['books'])
        // AppComponent.login = true
        // console.log(AppComponent.login)
      },
      error: (error)=>{
        console.log(error)
        const msg : { [key: string]: any } = error
        alert(msg['message'])
        this.form.reset()
        Object.keys(this.form.controls).forEach(key =>{
          this.form.controls[key].setErrors(null)
       });
      }
    }
    )
    // this.clicked = true
    // this.form.reset()
    // this.router.navigate(['books'])
  }

}