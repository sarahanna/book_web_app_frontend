import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AppComponent } from "src/app/app.component";
import { UserService } from "../users.service";

@Component({
  selector:'app-signup',
  templateUrl:'./signup.component.html',
  styleUrls:['./signup.component.css']
})
export class SignupComponent implements OnInit{
  form! : FormGroup
  constructor(public UserService:UserService,private router:Router){}
  ngOnInit(){
    this.form = new FormGroup({
    author:new FormControl(null,{validators:[Validators.required]}),
    password:new FormControl(null,{validators:[Validators.required]}),
    email:new FormControl(null,{validators:[Validators.required,Validators.email]}),

    })
  }

  onSignup()
  {

    console.log('hello')
    console.log(this.form)
    const author = {
      author:this.form.value.author,
      email:this.form.value.email,
      password:this.form.value.password

    }
    this.UserService.signup(author)
    this.form.reset()
    Object.keys(this.form.controls).forEach(key =>{
      this.form.controls[key].setErrors(null)
    })

  }
  loginNavigate()
  {
    this.router.navigate(['/login'])
  }

}