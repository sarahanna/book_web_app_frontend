import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateBookComponent } from './books/create-book/create-book.component';
import { DetailComponent } from './books/detail-book/detail-book.component';
import { ListBookComponent } from './books/list-book/list-book.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './users/login/login.component';
import { SignupComponent } from './users/signup/signup.component';



//routes : js object where we define for which the url , what part of our app should be presented

// const route : an array of objects , object with properties path, whta should get loaded in that path
const routes : Routes = [
  {path:'',component:HomeComponent},
  {path:'signup',component:SignupComponent},
  {path:'login',component:LoginComponent},
  {path:'books',component:ListBookComponent},
  {path:'create',component:CreateBookComponent},
  {path:'edit/:id',component:CreateBookComponent},
  {path:'detail/:id',component:DetailComponent}
]

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule{

}
